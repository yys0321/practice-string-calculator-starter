package com.tw;

public class StringCalculator {
    public int add(String string) {
        int total = 0;
        String[] numbers = string.replaceAll("[^0-9]+", ",").split(",");
        for (String number : numbers)
            if (!number.isEmpty())
                total += Integer.parseInt(number);
        return total;
    }
}
